import { TaskSchema } from './task.model';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { TasksController } from './task.controllers';
import { TaskService } from './task.service';

@Module({
  imports: [MongooseModule.forFeature([{name : 'task', schema:TaskSchema }])],
  controllers: [TasksController],
  providers: [TaskService],
})
export class TaskModule {}
