import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
} from '@nestjs/common';

import { TaskService } from './task.service';

@Controller('tasks')
export class TasksController {
  constructor(private readonly taskService: TaskService) { }

  @Post()
 async  addProduct(
    @Body('content') taskContent: string,
    @Body('description') taskDesc: string,
    @Body('isActive') taskActivation: boolean,
  ) {
    const  generatedId = await this.taskService.insertTask(
      taskContent,
      taskDesc,
      taskActivation,
    );
    return { id: generatedId };
  }

  @Get()
  async getAllgetTasks() {
    const res =  await this.taskService.getTasks();
    return res
  }

  @Get(':id')
  getTask(@Param('id') taskId: string) {
    return this.taskService.getSingleTask(taskId);
  }

  @Patch(':id')
  async updateTask(
    @Param('id') taskId: string,
    @Body('content') content: string,
    @Body('description') taskDesc: string,
  ) {
    await  this.taskService.updateTask(taskId, content, taskDesc);
    return null;
  }

   @Patch(':id/deactivate')
  async deactivateupdateTask(
    @Param('id') taskId: string,
    
  ) {
    await  this.taskService.deactivateTask(taskId);
    return null;
  }

  @Delete(':id')
  async removeProduct(@Param('id') taskId: string) {
   await  this.taskService.deleteTask(taskId);
    return null;
  }
}