

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task } from './task.model';


@Injectable()
export class TaskService {
  constructor(@InjectModel('task') private readonly taskModel: Model<Task>)
  {}

  async insertTask(content: string, desc: string, active: boolean) {
    const newTask = new this.taskModel({content,description : desc, isActive:active});
    
   const res = await newTask.save()
    return res.id;
  }

  async getTasks() {
    const tasks = await  this.taskModel.find().exec();
    return   tasks.map((task) => ({
        id: task.id, 
        content :  task.content,
        description : task.description,
        isActive: task.isActive
    }));
  }

 async getSingleTask(taskId: string) {
    const task = await this.findTask(taskId);
    return {
        id: task.id, 
        content :  task.content,
        description : task.description,
        isActive: task.isActive
    };
  }

  async updateTask(taskId: string, content: string, desc: string) {
    const updatedtask = await this.findTask(taskId);
   
    if (content) {
      updatedtask.content = content;
    }
    if (desc) {
      updatedtask.description = desc;
    }
     updatedtask.save();
  }
    async deactivateTask(taskId: string) {
    const deactivatedTask = await this.findTask(taskId);
    deactivatedTask.isActive = false;
    deactivatedTask.save();
  }

 async  deleteTask(taskId: string) {
   const res = await this.taskModel.deleteOne({_id: taskId}).exec();
   if (res.deletedCount === 0 ) {
       throw new NotFoundException('could not find the task you want');
   }
  }


// helper method
  private async findTask(id: string): Promise<Task> {

    let task;
    try {
         task = await this.taskModel.findById(id);
    } catch (error) {
        throw new NotFoundException('could not find a task with the given id')   
    }
    if (!task) {
      throw new NotFoundException('Could not find task.');
    }
    return task ;
  }
}
