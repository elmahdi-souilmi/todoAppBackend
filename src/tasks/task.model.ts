import * as mongoose from 'mongoose';



export const TaskSchema = new mongoose.Schema({
  content: { type: String, required: true },
  description: { type: String, required: true },
  isActive: { type: Boolean, required: true },
});


export interface Task extends mongoose.Document {
   id:string;
   content: string;
   description: string;
   isActive: boolean;
}
